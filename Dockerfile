﻿FROM mcr.microsoft.com/dotnet/sdk:latest AS build
WORKDIR /app

COPY *.sln .
COPY DatabaseHW.Core/*.csproj ./DatabaseHW.Core/
COPY DatabaseHW.DataAccess/*.csproj ./DatabaseHW.DataAccess/
COPY DatabaseHW.DataGenerator/*.csproj ./DatabaseHW.DataGenerator/
COPY DatabaseHW.WebApp/*.csproj ./DatabaseHW.WebApp/
RUN dotnet restore

COPY DatabaseHW.Core/. ./DatabaseHW.Core/
COPY DatabaseHW.DataAccess/. ./DatabaseHW.DataAccess/
COPY DatabaseHW.DataGenerator/. ./DatabaseHW.DataGenerator/
COPY DatabaseHW.WebApp/. ./DatabaseHW.WebApp/
WORKDIR /app/DatabaseHW.WebApp
# -r alpine-x64
RUN dotnet publish -r alpine-arm64 -c Release --self-contained true /p:PublishSingleFile=true /p:PublishTrimmed=true -o out

FROM alpine:latest AS runtime
RUN apk add --no-cache libstdc++ krb5-libs
EXPOSE 80
WORKDIR /app
COPY --from=build /app/DatabaseHW.WebApp/out ./
ENTRYPOINT ["./DatabaseHW.WebApp", "--urls", "http://0.0.0.0:80"]
