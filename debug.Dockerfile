FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /app

COPY *.sln .
COPY DatabaseHW.Core/*.csproj ./DatabaseHW.Core/
COPY DatabaseHW.DataAccess/*.csproj ./DatabaseHW.DataAccess/
COPY DatabaseHW.DataGenerator/*.csproj ./DatabaseHW.DataGenerator/
COPY DatabaseHW.WebApp/*.csproj ./DatabaseHW.WebApp/
RUN dotnet restore

COPY DatabaseHW.Core/. ./DatabaseHW.Core/
COPY DatabaseHW.DataAccess/. ./DatabaseHW.DataAccess/
COPY DatabaseHW.DataGenerator/. ./DatabaseHW.DataGenerator/
COPY DatabaseHW.WebApp/. ./DatabaseHW.WebApp/
WORKDIR /app/DatabaseHW.WebApp
RUN dotnet publish -c Debug -o out

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS runtime
WORKDIR /app
COPY --from=build /app/DatabaseHW.WebApp/out ./
ENTRYPOINT ["dotnet", "DatabaseHW.WebApp.dll"]
