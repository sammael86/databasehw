﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DatabaseHW.Core.Repositories
{
    public interface IRepository<T>
        where T : class
    {
        Task<T> GetAsync(int id);
        Task<IEnumerable<T>> GetAllAsync();
        Task AddAsync(T item);
        Task DeleteAsync(T item);
        Task SaveAsync();
    }
}