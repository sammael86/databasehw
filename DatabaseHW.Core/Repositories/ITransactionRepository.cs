﻿using DatabaseHW.Core.Entities;

namespace DatabaseHW.Core.Repositories
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
        
    }
}