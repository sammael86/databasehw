﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;

namespace DatabaseHW.Core.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        Task<IEnumerable<Account>> GetUserAccountsAsync(int id);
        Task<IEnumerable<Card>> GetUserCardsAsync(int id);
    }
}