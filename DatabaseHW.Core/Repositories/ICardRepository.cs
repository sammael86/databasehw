﻿using System.Threading.Tasks;
using DatabaseHW.Core.Entities;

namespace DatabaseHW.Core.Repositories
{
    public interface ICardRepository : IRepository<Card>
    {
        Task<Account> GetCardAccountAsync(int id);
    }
}