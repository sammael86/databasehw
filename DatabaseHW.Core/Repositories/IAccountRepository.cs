﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;

namespace DatabaseHW.Core.Repositories
{
    public interface IAccountRepository : IRepository<Account>
    {
        Task<IEnumerable<Transaction>> GetAccountTransactionsAsync(int id, DateTime fromDate, DateTime dueDate);
        Task<IEnumerable<Card>> GetAccountCardsAsync(int id);
    }
}