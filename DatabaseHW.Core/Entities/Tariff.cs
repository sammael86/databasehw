﻿namespace DatabaseHW.Core.Entities
{
    public class Tariff
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}