﻿namespace DatabaseHW.Core.Entities
{
    public class CardType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}