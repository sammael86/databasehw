﻿using System;

namespace DatabaseHW.Core.Entities
{
    public class Transaction
    {
        public int Id { get; set; }
        public DateTime TransactionDateTime { get; set; }
        public int TransactionTypeId { get; set; }
        public decimal Sum { get; set; }
        public int CurrencyId { get; set; }
        public decimal Rate { get; set; }
        public int AccountId { get; set; }
    }
}