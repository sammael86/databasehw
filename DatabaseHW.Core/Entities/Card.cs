﻿namespace DatabaseHW.Core.Entities
{
    public class Card
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
        public int CardTypeId { get; set; }
        public int AccountId { get; set; }
    }
}