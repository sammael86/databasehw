﻿namespace DatabaseHW.Core.Entities
{
    public class Currency
    {
        public int Id { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
    }
}