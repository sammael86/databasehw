﻿using System;

namespace DatabaseHW.Core.Entities
{
    public class Account
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime OpenDate { get; set; }
        public int UserId { get; set; }
        public int CurrencyId { get; set; }
        public int TariffId { get; set; }
        public decimal Balance { get; set; }
    }
}