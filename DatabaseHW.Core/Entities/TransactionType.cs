﻿namespace DatabaseHW.Core.Entities
{
    public class TransactionType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}