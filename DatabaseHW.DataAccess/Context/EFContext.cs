﻿using DatabaseHW.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace DatabaseHW.DataAccess.Context
{
    public sealed class EFContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<CardType> CardTypes { get; set; }
        public DbSet<TransactionType> TransactionTypes { get; set; }

        public EFContext(DbContextOptions<EFContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Currency>().HasData(
                new Currency {Id = 1, Code = 643, Name = "RUB", Rate = 1m},
                new Currency {Id = 2, Code = 840, Name = "USD", Rate = 76.0120m},
                new Currency {Id = 3, Code = 978, Name = "EUR", Rate = 90.2643m}
            );

            modelBuilder.Entity<Tariff>().HasData(
                new Tariff {Id = 1, Name = "UNIVERSAL", Description = "Универсальный тариф"},
                new Tariff {Id = 2, Name = "DEPOSIT-3", Description = "Накопительный 3% годовых"},
                new Tariff {Id = 3, Name = "ON-DEMAND", Description = "До востребования 0,1% годовых"}
            );

            modelBuilder.Entity<CardType>().HasData(
                new CardType {Id = 1, Name = "Visa"},
                new CardType {Id = 2, Name = "MasterCard"},
                new CardType {Id = 3, Name = "MIR"}
            );

            modelBuilder.Entity<TransactionType>().HasData(
                new TransactionType {Id = 1, Name = "In"},
                new TransactionType {Id = 2, Name = "Out"}
            );
        }
    }
}