﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DatabaseHW.DataAccess.Repositories.ADO
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IOptions<ConnectionStrings> options, IDistributedCache distributedCache,
            ILogger<Repository<User>> logger) : base(options, distributedCache, logger)
        {
        }

        public async Task<IEnumerable<Account>> GetUserAccountsAsync(int id)
        {
            var key = $"User/{nameof(id)}/{id}/Accounts";
            IEnumerable<Account> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<Account>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");
                var accounts = new List<Account>();
                var sql = $"SELECT * FROM Accounts WHERE UserId = @id";

                await using var connection = new MySqlConnection(ConnectionString);
                await connection.OpenAsync();
                await using var command = new MySqlCommand(sql, connection);
                command.Parameters.Add(new MySqlParameter("@id", id));
                await using var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        accounts.Add(new Account()
                        {
                            Id = reader.GetInt32(reader.GetOrdinal(nameof(Account.Id))),
                            Balance = reader.GetDecimal(reader.GetOrdinal(nameof(Account.Balance))),
                            CurrencyId = reader.GetInt32(reader.GetOrdinal(nameof(Account.CurrencyId))),
                            Number = reader.GetString(reader.GetOrdinal(nameof(Account.Number))),
                            OpenDate = reader.GetDateTime(reader.GetOrdinal(nameof(Account.OpenDate))),
                            TariffId = reader.GetInt32(reader.GetOrdinal(nameof(Account.TariffId))),
                            UserId = reader.GetInt32(reader.GetOrdinal(nameof(Account.UserId)))
                        });
                    }
                }

                list = accounts;
                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }

        public async Task<IEnumerable<Card>> GetUserCardsAsync(int id)
        {
            var key = $"User/{nameof(id)}/{id}/Cards";
            IEnumerable<Card> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<Card>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");
                var cards = new List<Card>();
                var sql = $"SELECT C.* FROM Cards C INNER JOIN Accounts A ON C.AccountId = A.Id WHERE A.UserId = @id";

                await using var connection = new MySqlConnection(ConnectionString);
                await connection.OpenAsync();
                await using var command = new MySqlCommand(sql, connection);
                command.Parameters.Add(new MySqlParameter("@id", id));
                await using var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        cards.Add(new Card()
                        {
                            Id = reader.GetInt32(reader.GetOrdinal(nameof(Card.Id))),
                            Name = reader.GetString(reader.GetOrdinal(nameof(Card.Name))),
                            Number = reader.GetString(reader.GetOrdinal(nameof(Card.Number))),
                            CardTypeId = reader.GetInt32(reader.GetOrdinal(nameof(Card.CardTypeId))),
                            AccountId = reader.GetInt32(reader.GetOrdinal(nameof(Card.AccountId)))
                        });
                    }
                }

                list = cards;
                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }

        public override async Task AddAsync(User user)
        {
            var sql =
                $"INSERT INTO Users (FirstName, LastName, MiddleName, Phone, Email, Passport) " +
                $"VALUES (@firstName, @lastName, @middleName, @phone, @email, @passport)";

            await using var connection = new MySqlConnection(ConnectionString);
            await connection.OpenAsync();
            await using var command = new MySqlCommand(sql, connection);
            command.Parameters.Add(new MySqlParameter("@firstName", user.FirstName));
            command.Parameters.Add(new MySqlParameter("@lastName", user.LastName));
            command.Parameters.Add(new MySqlParameter("@middleName", user.MiddleName));
            command.Parameters.Add(new MySqlParameter("@phone", user.Phone));
            command.Parameters.Add(new MySqlParameter("@email", user.Email));
            command.Parameters.Add(new MySqlParameter("@passport", user.Passport));
            await command.ExecuteNonQueryAsync();
        }
    }
}