﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DatabaseHW.DataAccess.Repositories.ADO
{
    public class AccountRepository : Repository<Account>, IAccountRepository
    {
        public AccountRepository(IOptions<ConnectionStrings> options, IDistributedCache distributedCache,
            ILogger<Repository<Account>> logger) : base(options, distributedCache, logger)
        {
        }

        public async Task<IEnumerable<Transaction>> GetAccountTransactionsAsync(int id, DateTime fromDate,
            DateTime dueDate)
        {
            var key = $"Account/{nameof(id)}/{id}/Transactions/{fromDate:s}-{dueDate:s}";
            IEnumerable<Transaction> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<Transaction>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");

                var transactions = new List<Transaction>();
                var sql =
                    $"SELECT * FROM Transactions WHERE AccountId = @id AND TransactionDateTime BETWEEN @fromDate AND @dueDate";

                await using var connection = new MySqlConnection(ConnectionString);
                await connection.OpenAsync();
                await using var command = new MySqlCommand(sql, connection);
                command.Parameters.Add(new MySqlParameter("@id", id));
                command.Parameters.Add(new MySqlParameter("@fromDate", fromDate));
                command.Parameters.Add(new MySqlParameter("@dueDate", dueDate));
                await using var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        transactions.Add(new Transaction()
                        {
                            Id = reader.GetInt32(reader.GetOrdinal(nameof(Transaction.Id))),
                            TransactionDateTime =
                                reader.GetDateTime(reader.GetOrdinal(nameof(Transaction.TransactionDateTime))),
                            TransactionTypeId =
                                reader.GetInt32(reader.GetOrdinal(nameof(Transaction.TransactionTypeId))),
                            Sum = reader.GetDecimal(reader.GetOrdinal(nameof(Transaction.Sum))),
                            CurrencyId = reader.GetInt32(reader.GetOrdinal(nameof(Transaction.CurrencyId))),
                            Rate = reader.GetDecimal(reader.GetOrdinal(nameof(Transaction.Rate))),
                            AccountId = reader.GetInt32(reader.GetOrdinal(nameof(Transaction.AccountId)))
                        });
                    }
                }

                list = transactions;
                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }

        public async Task<IEnumerable<Card>> GetAccountCardsAsync(int id)
        {
            var key = $"Account/{nameof(id)}/{id}/Cards";
            IEnumerable<Card> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<Card>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");

                var cards = new List<Card>();
                var sql = $"SELECT * FROM Cards WHERE AccountId = @id";

                await using var connection = new MySqlConnection(ConnectionString);
                await connection.OpenAsync();
                await using var command = new MySqlCommand(sql, connection);
                command.Parameters.Add(new MySqlParameter("@id", id));
                await using var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        cards.Add(new Card()
                        {
                            Id = reader.GetInt32(reader.GetOrdinal(nameof(Card.Id))),
                            Name = reader.GetString(reader.GetOrdinal(nameof(Card.Name))),
                            Number = reader.GetString(reader.GetOrdinal(nameof(Card.Number))),
                            CardTypeId = reader.GetInt32(reader.GetOrdinal(nameof(Card.CardTypeId))),
                            AccountId = reader.GetInt32(reader.GetOrdinal(nameof(Card.AccountId)))
                        });
                    }
                }

                list = cards;
                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }

        public override async Task AddAsync(Account account)
        {
            var sql =
                $"INSERT INTO Accounts (Number, OpenDate, UserId, CurrencyId, TariffId, Balance) " +
                $"VALUES (@number, @openDate, @userId, @currencyId, @tariffId, @balance)";

            await using var connection = new MySqlConnection(ConnectionString);
            await connection.OpenAsync();
            await using var command = new MySqlCommand(sql, connection);
            command.Parameters.Add(new MySqlParameter("@number", account.Number));
            command.Parameters.Add(new MySqlParameter("@openDate", account.OpenDate));
            command.Parameters.Add(new MySqlParameter("@userId", account.UserId));
            command.Parameters.Add(new MySqlParameter("@currencyId", account.CurrencyId));
            command.Parameters.Add(new MySqlParameter("@tariffId", account.TariffId));
            command.Parameters.Add(new MySqlParameter("@balance", account.Balance));
            await command.ExecuteNonQueryAsync();
        }

        public override async Task DeleteAsync(Account account)
        {
            var sql = $"DELETE FROM Cards WHERE AccountId=@id;" +
                      $"DELETE FROM Transactions WHERE AccountId=@id;" +
                      $"DELETE FROM Accounts WHERE Id=@id;";

            await using var connection = new MySqlConnection(ConnectionString);
            await connection.OpenAsync();
            await using var command = new MySqlCommand(sql, connection);
            command.Parameters.Add(new MySqlParameter("@id", account.Id));
            await command.ExecuteNonQueryAsync();
        }
    }
}