﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DatabaseHW.DataAccess.Repositories.ADO
{
    public class Repository<T> : IRepository<T>
        where T : class
    {
        protected readonly string ConnectionString;
        protected readonly IDistributedCache DistributedCache;
        protected readonly ILogger<Repository<T>> Logger;

        public Repository(IOptions<ConnectionStrings> options, IDistributedCache distributedCache,
            ILogger<Repository<T>> logger)
        {
            ConnectionString = options.Value.MySQLConnection;
            DistributedCache = distributedCache;
            Logger = logger;
        }

        public async Task<T> GetAsync(int id)
        {
            var key = $"{typeof(T).Name}/{nameof(id)}/{id}";
            T item;
            string serializedItem;

            var cachedItem = await DistributedCache.GetAsync(key);
            if (cachedItem != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedItem = Encoding.UTF8.GetString(cachedItem);
                item = JsonConvert.DeserializeObject<T>(serializedItem);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");

                var sql = $"SELECT * FROM {typeof(T).Name}s WHERE Id = @id";
                var sb = new StringBuilder();

                await using var connection = new MySqlConnection(ConnectionString);
                await connection.OpenAsync();
                await using var command = new MySqlCommand(sql, connection);
                command.Parameters.Add(new MySqlParameter("@id", id));
                await using var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        if (reader.CanGetColumnSchema())
                        {
                            var columnSchema = await reader.GetColumnSchemaAsync();
                            sb.Append("{");
                            foreach (var column in columnSchema)
                            {
                                sb.Append($"\"{column.ColumnName}\":");
                                sb.Append($"\"{reader.GetString(reader.GetOrdinal(column.ColumnName))}\"");
                                sb.Append(",");
                            }

                            sb.Remove(sb.Length - 1, 1);
                            sb.Append("}");
                        }
                    }
                }

                item = JsonConvert.DeserializeObject<T>(sb.ToString());

                serializedItem = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(item));
                cachedItem = Encoding.UTF8.GetBytes(serializedItem);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedItem, options);
            }

            return item;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            var key = $"{typeof(T).Name}/All";
            IEnumerable<T> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<T>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");

                var sql = $"SELECT * FROM {typeof(T).Name}s";
                var sb = new StringBuilder();

                await using var connection = new MySqlConnection(ConnectionString);
                await connection.OpenAsync();
                await using var command = new MySqlCommand(sql, connection);
                await using var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    sb.Append("[");
                    while (await reader.ReadAsync())
                    {
                        if (reader.CanGetColumnSchema())
                        {
                            var columnSchema = await reader.GetColumnSchemaAsync();
                            sb.Append("{");
                            foreach (var column in columnSchema)
                            {
                                sb.Append($"\"{column.ColumnName}\":");
                                sb.Append($"\"{reader.GetString(reader.GetOrdinal(column.ColumnName))}\"");
                                sb.Append(",");
                            }

                            sb.Remove(sb.Length - 1, 1);
                            sb.Append("}");
                        }

                        sb.Append(",");
                    }
                    
                    sb.Remove(sb.Length - 1, 1);
                    sb.Append("]");
                }

                list = JsonConvert.DeserializeObject<IEnumerable<T>>(sb.ToString());

                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }

        public virtual async Task AddAsync(T item)
        {
        }

        public virtual async Task DeleteAsync(T item)
        {
        }

        public async Task SaveAsync()
        {
        }
    }
}