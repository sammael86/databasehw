﻿using System;
using System.Text;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DatabaseHW.DataAccess.Repositories.ADO
{
    public class CardRepository : Repository<Card>, ICardRepository
    {
        public CardRepository(IOptions<ConnectionStrings> options, IDistributedCache distributedCache,
            ILogger<Repository<Card>> logger) : base(options, distributedCache, logger)
        {
        }

        public async Task<Account> GetCardAccountAsync(int id)
        {
            var key = $"Card/{nameof(id)}/{id}/Account";
            Account account = null;
            string serializedAccount;

            var cached = await DistributedCache.GetAsync(key);
            if (cached != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedAccount = Encoding.UTF8.GetString(cached);
                account = JsonConvert.DeserializeObject<Account>(serializedAccount);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");

                var sql = $"SELECT A.* FROM Accounts A INNER JOIN Cards C on A.Id = C.AccountId WHERE C.Id = @id";

                await using var connection = new MySqlConnection(ConnectionString);
                await connection.OpenAsync();
                await using var command = new MySqlCommand(sql, connection);
                command.Parameters.Add(new MySqlParameter("@id", id));
                await using var reader = await command.ExecuteReaderAsync();
                if (reader.HasRows)
                {
                    await reader.ReadAsync();

                    account = new Account()
                    {
                        Id = reader.GetInt32(reader.GetOrdinal(nameof(Account.Id))),
                        Balance = reader.GetDecimal(reader.GetOrdinal(nameof(Account.Balance))),
                        CurrencyId = reader.GetInt32(reader.GetOrdinal(nameof(Account.CurrencyId))),
                        Number = reader.GetString(reader.GetOrdinal(nameof(Account.Number))),
                        OpenDate = reader.GetDateTime(reader.GetOrdinal(nameof(Account.OpenDate))),
                        TariffId = reader.GetInt32(reader.GetOrdinal(nameof(Account.TariffId))),
                        UserId = reader.GetInt32(reader.GetOrdinal(nameof(Account.UserId)))
                    };
                }

                serializedAccount = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(account));
                cached = Encoding.UTF8.GetBytes(serializedAccount);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cached, options);
            }

            return account;
        }

        public override async Task AddAsync(Card card)
        {
            var sql =
                $"INSERT INTO Cards (Name, Number, CardTypeId, AccountId) " +
                $"VALUES (@name, @number, @cardTypeId, @accountId)";

            await using var connection = new MySqlConnection(ConnectionString);
            await connection.OpenAsync();
            await using var command = new MySqlCommand(sql, connection);
            command.Parameters.Add(new MySqlParameter("@name", card.Name));
            command.Parameters.Add(new MySqlParameter("@number", card.Number));
            command.Parameters.Add(new MySqlParameter("@cardTypeId", card.CardTypeId));
            command.Parameters.Add(new MySqlParameter("@accountId", card.AccountId));
            await command.ExecuteNonQueryAsync();
        }

        public override async Task DeleteAsync(Card card)
        {
            var sql = $"DELETE FROM Cards WHERE Id=@id";

            await using var connection = new MySqlConnection(ConnectionString);
            await connection.OpenAsync();
            await using var command = new MySqlCommand(sql, connection);
            command.Parameters.Add(new MySqlParameter("@id", card.Id));
            await command.ExecuteNonQueryAsync();
        }
    }
}