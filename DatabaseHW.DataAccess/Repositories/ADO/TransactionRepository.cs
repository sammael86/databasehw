﻿using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;

namespace DatabaseHW.DataAccess.Repositories.ADO
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(IOptions<ConnectionStrings> options, IDistributedCache distributedCache,
            ILogger<Repository<Transaction>> logger) : base(options, distributedCache, logger)
        {
        }

        public override async Task AddAsync(Transaction transaction)
        {
            var sql =
                $"INSERT INTO Transactions (TransactionDateTime, TransactionTypeId, Sum, CurrencyId, Rate, AccountId) " +
                $"VALUES (@transactionDateTime, @transactionTypeId, @sum, @currencyId, @rate, @accountId);" +
                $"UPDATE Accounts SET Balance = Balance + @diff WHERE Id=@accountId;";

            await using var connection = new MySqlConnection(ConnectionString);
            await connection.OpenAsync();
            await using var command = new MySqlCommand(sql, connection);
            command.Parameters.Add(new MySqlParameter("@transactionDateTime", transaction.TransactionDateTime));
            command.Parameters.Add(new MySqlParameter("@transactionTypeId", transaction.TransactionTypeId));
            command.Parameters.Add(new MySqlParameter("@sum", transaction.Sum));
            command.Parameters.Add(new MySqlParameter("@currencyId", transaction.CurrencyId));
            command.Parameters.Add(new MySqlParameter("@rate", transaction.Rate));
            command.Parameters.Add(new MySqlParameter("@accountId", transaction.AccountId));
            command.Parameters.Add(new MySqlParameter("@diff",
                transaction.Sum * transaction.Rate * (transaction.TransactionTypeId == 1 ? 1 : -1)));
            await command.ExecuteNonQueryAsync();
        }

        public override async Task DeleteAsync(Transaction transaction)
        {
            var sql =
                $"DELETE FROM Transactions WHERE Id=@id;" +
                $"UPDATE Accounts SET Balance = Balance - @diff WHERE Id=@accountId;";

            await using var connection = new MySqlConnection(ConnectionString);
            await connection.OpenAsync();
            await using var command = new MySqlCommand(sql, connection);
            command.Parameters.Add(new MySqlParameter("@id", transaction.Id));
            command.Parameters.Add(new MySqlParameter("@accountId", transaction.AccountId));
            command.Parameters.Add(new MySqlParameter("@diff",
                transaction.Sum * transaction.Rate * (transaction.TransactionTypeId == 1 ? 1 : -1)));
            await command.ExecuteNonQueryAsync();
        }
    }
}