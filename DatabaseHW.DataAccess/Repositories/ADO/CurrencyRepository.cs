﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.DataAccess.Context;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace DatabaseHW.DataAccess.Repositories.ADO
{
    public class CurrencyRepository : Repository<Currency>
    {
        public CurrencyRepository(IOptions<ConnectionStrings> options, IDistributedCache distributedCache,
            ILogger<Repository<Currency>> logger) : base(options, distributedCache, logger)
        {
        }

        public override async Task<IEnumerable<Currency>> GetAllAsync()
        {
            var key = $"Currency/All";
            IEnumerable<Currency> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<Currency>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");
                var currencies = new List<Currency>();
                var sql = $"SELECT * FROM Currencies";

                await using var connection = new MySqlConnection(ConnectionString);
                await connection.OpenAsync();
                await using var command = new MySqlCommand(sql, connection);
                await using var reader = await command.ExecuteReaderAsync();

                if (reader.HasRows)
                {
                    while (await reader.ReadAsync())
                    {
                        currencies.Add(new Currency()
                        {
                            Id = reader.GetInt32(reader.GetOrdinal(nameof(Currency.Id))),
                            Code = reader.GetInt32(reader.GetOrdinal(nameof(Currency.Code))),
                            Name = reader.GetString(reader.GetOrdinal(nameof(Currency.Name))),
                            Rate = reader.GetDecimal(reader.GetOrdinal(nameof(Currency.Rate)))
                        });
                    }
                }

                list = currencies;

                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }
    }
}