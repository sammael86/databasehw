﻿using System;
using System.Text;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DatabaseHW.DataAccess.Repositories.EF
{
    public class CardRepository : Repository<Card>, ICardRepository
    {
        public CardRepository(EFContext context, IDistributedCache distributedCache, ILogger<CardRepository> logger)
            : base(context, distributedCache, logger)
        {
        }

        public async Task<Account> GetCardAccountAsync(int id)
        {
            var key = $"Card/{nameof(id)}/{id}/Account";
            Account account;
            string serializedAccount;

            var cached = await DistributedCache.GetAsync(key);
            if (cached != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedAccount = Encoding.UTF8.GetString(cached);
                account = JsonConvert.DeserializeObject<Account>(serializedAccount);
            }
            else
            {
                var card = await GetAsync(id);
                Logger.LogInformation($"Получаем {key} из БД");
                account = await Context.Accounts.FirstOrDefaultAsync(a => a.Id == card.AccountId);
                serializedAccount = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(account));
                cached = Encoding.UTF8.GetBytes(serializedAccount);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cached, options);
            }

            return account;
        }
    }
}