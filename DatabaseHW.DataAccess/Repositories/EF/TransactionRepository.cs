﻿using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;

namespace DatabaseHW.DataAccess.Repositories.EF
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(EFContext context, IDistributedCache distributedCache,
            ILogger<TransactionRepository> logger)
            : base(context, distributedCache, logger)
        {
        }

        public override async Task AddAsync(Transaction transaction)
        {
            var account = await Context.Accounts.FirstOrDefaultAsync(a => a.Id == transaction.AccountId);
            if (account is null)
                return;

            await base.AddAsync(transaction);

            account.Balance += transaction.Sum * transaction.Rate * (transaction.TransactionTypeId == 1 ? 1 : -1);
        }

        public override async Task DeleteAsync(Transaction transaction)
        {
            await base.DeleteAsync(transaction);

            var account = await Context.Accounts.FirstOrDefaultAsync(a => a.Id == transaction.AccountId);
            if (account is null)
                return;

            account.Balance -= transaction.Sum * transaction.Rate * (transaction.TransactionTypeId == 1 ? 1 : -1);
        }
    }
}