﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DatabaseHW.DataAccess.Repositories.EF
{
    public class AccountRepository : Repository<Account>, IAccountRepository
    {
        public AccountRepository(EFContext context, IDistributedCache distributedCache,
            ILogger<AccountRepository> logger)
            : base(context, distributedCache, logger)
        {
        }

        public async Task<IEnumerable<Transaction>> GetAccountTransactionsAsync(int id, DateTime fromDate,
            DateTime dueDate)
        {
            var key = $"Account/{nameof(id)}/{id}/Transactions/{fromDate:s}-{dueDate:s}";
            IEnumerable<Transaction> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<Transaction>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");
                list = Context.Transactions.Where(t =>
                    t.AccountId == id && t.TransactionDateTime >= fromDate && t.TransactionDateTime <= dueDate);
                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }

        public async Task<IEnumerable<Card>> GetAccountCardsAsync(int id)
        {
            var key = $"Account/{nameof(id)}/{id}/Cards";
            IEnumerable<Card> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<Card>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");
                list = Context.Cards.Where(c => c.AccountId == id);
                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }

        public override async Task DeleteAsync(Account account)
        {
            var cards = await GetAccountCardsAsync(account.Id);
            Context.Cards.RemoveRange(cards);

            var transactions = await GetAccountTransactionsAsync(account.Id, account.OpenDate, DateTime.Now);
            Context.Transactions.RemoveRange(transactions);

            await base.DeleteAsync(account);
        }
    }
}