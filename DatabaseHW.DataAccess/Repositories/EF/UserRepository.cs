﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DatabaseHW.DataAccess.Repositories.EF
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(EFContext context, IDistributedCache distributedCache, ILogger<UserRepository> logger)
            : base(context, distributedCache, logger)
        {
        }

        public async Task<IEnumerable<Account>> GetUserAccountsAsync(int id)
        {
            var key = $"User/{nameof(id)}/{id}/Accounts";
            IEnumerable<Account> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<Account>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");
                list = Context.Accounts.Where(a => a.UserId == id);
                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }

        public async Task<IEnumerable<Card>> GetUserCardsAsync(int id)
        {
            var key = $"User/{nameof(id)}/{id}/Cards";
            IEnumerable<Card> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<Card>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");
                var accounts = await GetUserAccountsAsync(id);
                list = Context.Cards.Where(c => accounts.Select(a => a.Id).Contains(c.AccountId));
                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }
    }
}