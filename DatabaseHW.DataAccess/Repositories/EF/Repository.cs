﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DatabaseHW.DataAccess.Repositories.EF
{
    public class Repository<T> : IRepository<T>
        where T : class
    {
        protected readonly EFContext Context;
        protected readonly DbSet<T> DbSet;
        protected readonly IDistributedCache DistributedCache;
        protected readonly ILogger<Repository<T>> Logger;

        public Repository(EFContext context, IDistributedCache distributedCache, ILogger<Repository<T>> logger)
        {
            Context = context;
            DbSet = Context.Set<T>();
            DistributedCache = distributedCache;
            Logger = logger;
        }

        public async Task<T> GetAsync(int id)
        {
            var key = $"{typeof(T).Name}/{nameof(id)}/{id}";
            T item;
            string serializedItem;

            var cachedItem = await DistributedCache.GetAsync(key);
            if (cachedItem != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedItem = Encoding.UTF8.GetString(cachedItem);
                item = JsonConvert.DeserializeObject<T>(serializedItem);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");
                item = await DbSet.FindAsync(id);
                serializedItem = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(item));
                cachedItem = Encoding.UTF8.GetBytes(serializedItem);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedItem, options);
            }

            return item;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var key = $"{typeof(T).Name}/All";
            IEnumerable<T> list;
            string serializedList;

            var cachedList = await DistributedCache.GetAsync(key);
            if (cachedList != null)
            {
                Logger.LogInformation($"Получаем {key} из кэша");
                serializedList = Encoding.UTF8.GetString(cachedList);
                list = JsonConvert.DeserializeObject<IEnumerable<T>>(serializedList);
            }
            else
            {
                Logger.LogInformation($"Получаем {key} из БД");
                list = Context.Set<T>();
                serializedList = await Task.Factory.StartNew(() => JsonConvert.SerializeObject(list));
                cachedList = Encoding.UTF8.GetBytes(serializedList);
                var options = new DistributedCacheEntryOptions()
                    .SetAbsoluteExpiration(DateTimeOffset.Now.AddMinutes(10))
                    .SetSlidingExpiration(TimeSpan.FromMinutes(2));
                await DistributedCache.SetAsync(key, cachedList, options);
            }

            return list;
        }

        public virtual async Task AddAsync(T item)
            => await DbSet.AddAsync(item);

        public virtual async Task DeleteAsync(T item)
            => await Task.Factory.StartNew(() => DbSet.Remove(item));

        public async Task SaveAsync()
            => await Context.SaveChangesAsync();
    }
}