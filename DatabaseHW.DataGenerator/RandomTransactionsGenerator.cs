﻿using System;
using System.Collections.Generic;
using Bogus;
using DatabaseHW.Core.Entities;

namespace DatabaseHW.DataGenerator
{
    public static class RandomTransactionsGenerator
    {
        public static List<Transaction> Generate(List<Account> accounts)
        {
            var transactions = new List<Transaction>();
            var r = new Random();

            foreach (var account in accounts)
            {
                var transactionsCount = r.Next(10, 100);
                transactions.AddRange(CreateFaker(transactionsCount, account));
            }

            return transactions;
        }

        private static List<Transaction> CreateFaker(int count, Account account)
        {
            var transactions = new List<Transaction>();

            for (int i = 0; i < count; i++)
            {
                var transactionFaker = new Faker<Transaction>()
                    .CustomInstantiator(f => new Transaction() {AccountId = account.Id})
                    .RuleFor(t => t.TransactionDateTime, (f, _) => f.Date.Between(account.OpenDate, DateTime.Now))
                    .RuleFor(t => t.TransactionTypeId, (f, _) => f.Random.Int(1, 2))
                    .RuleFor(t => t.Sum, (f, _) => f.Finance.Amount(0m, 10000m))
                    .RuleFor(t => t.CurrencyId,
                        (f, _) => f.Random.Int(0, 5) == 0 ? f.Random.Int(1, 3) : account.CurrencyId)
                    .RuleFor(t => t.Rate, (f, _) => f.Finance.Amount(60m, 95m));

                var transaction = (Transaction) transactionFaker;
                if (transaction.CurrencyId == account.CurrencyId)
                    transaction.Rate = 1m;

                transactions.Add(transaction);
            }

            return transactions;
        }
    }
}