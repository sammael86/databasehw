﻿using System;
using System.Collections.Generic;
using Bogus;
using DatabaseHW.Core.Entities;

namespace DatabaseHW.DataGenerator
{
    public static class RandomAccountsGenerator
    {
        public static List<Account> Generate(int count)
        {
            var accounts = new List<Account>();
            var r = new Random();

            for (int i = 0; i < count; i++)
            {
                var accountsCount = r.Next(1, 5);
                accounts.AddRange(CreateFaker(accountsCount, i + 1));
            }

            return accounts;
        }

        private static List<Account> CreateFaker(int count, int id)
        {
            var accounts = new List<Account>();

            for (int i = 0; i < count; i++)
            {
                var accountFaker = new Faker<Account>()
                    .CustomInstantiator(f => new Account() {UserId = id})
                    .RuleFor(a => a.Number, (f, _) => f.Finance.Account(20))
                    .RuleFor(a => a.CurrencyId, (f, _) => f.Random.Int(1, 3))
                    .RuleFor(a => a.OpenDate,
                        (f, _) => f.Date.Between(DateTime.Now.AddMonths(-100), DateTime.Now.AddMonths(-4)))
                    .RuleFor(a => a.TariffId, (f, _) => f.Random.Int(1, 3));
                accounts.Add(accountFaker);
            }

            return accounts;
        }
    }
}