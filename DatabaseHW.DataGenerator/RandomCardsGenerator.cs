﻿using System;
using System.Collections.Generic;
using Bogus;
using DatabaseHW.Core.Entities;

namespace DatabaseHW.DataGenerator
{
    public static class RandomCardsGenerator
    {
        public static List<Card> Generate(int count)
        {
            var cards = new List<Card>();
            var r = new Random();

            for (int i = 0; i < count; i++)
            {
                var cardsCount = r.Next(0, 4);
                if (cardsCount > 0)
                    cards.Add(CreateFaker(i + 1));
            }

            return cards;
        }

        private static Card CreateFaker(int accountId)
        {
            var cardFaker = new Faker<Card>()
                .CustomInstantiator(f => new Card() {AccountId = accountId})
                .RuleFor(c => c.Name, (f, _) => f.Finance.AccountName())
                .RuleFor(c => c.Number, (f, _) => f.Finance.CreditCardNumber())
                .RuleFor(c => c.CardTypeId, (f, _) => f.Random.Int(1, 3));

            return cardFaker;
        }
    }
}