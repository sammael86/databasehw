﻿using System.Collections.Generic;
using Bogus;
using DatabaseHW.Core.Entities;

namespace DatabaseHW.DataGenerator
{
    public static class RandomUsersGenerator
    {
        public static List<User> Generate(int count)
        {
            var users = new List<User>();
            var userFaker = CreateFaker();

            foreach (var user in userFaker.GenerateForever())
            {
                users.Add(user);

                if (count == users.Count)
                    break;
            }

            return users;
        }

        private static Faker<User> CreateFaker()
        {
            var userFaker = new Faker<User>()
                .CustomInstantiator(f => new User())
                .RuleFor(u => u.FirstName, (f, u) => f.Name.FirstName())
                .RuleFor(u => u.LastName, (f, u) => f.Name.LastName())
                .RuleFor(u => u.MiddleName, (f, u) => f.Name.LastName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FirstName, u.LastName))
                .RuleFor(u => u.Passport, (f, u) => f.Random.ReplaceNumbers("#### ######", '#'))
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("7-###-###-##-##"));

            return userFaker;
        }
    }
}