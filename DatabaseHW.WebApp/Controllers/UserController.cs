﻿using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DatabaseHW.WebApp.Controllers
{
    [ApiController]
    [Route("sberbank/users")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserRepository _userRepository;

        public UserController(ILogger<UserController> logger,
            IUserRepository userRepository)
            => (_logger, _userRepository) =
                (logger, userRepository);

        [HttpPost("add")]
        public async Task<ActionResult> AddUser(User user)
        {
            await _userRepository.AddAsync(user);
            await _userRepository.SaveAsync();

            return Ok();
        }

        [HttpGet("{id}/accounts")]
        public async Task<ActionResult> GetUserAccounts(int id)
        {
            var userAccountsList = await _userRepository.GetUserAccountsAsync(id);

            if (userAccountsList is null)
                return NotFound();

            return Ok(userAccountsList);
        }

        [HttpGet("{id}/cards")]
        public async Task<ActionResult> GetUserCards(int id)
        {
            var userCardsList = await _userRepository.GetUserCardsAsync(id);

            if (userCardsList is null)
                return NotFound();

            return Ok(userCardsList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            var user = await _userRepository.GetAsync(id);

            if (user is null)
                return NotFound();

            return Ok(user);
        }

        [HttpGet("")]
        public async Task<IActionResult> GetUsers()
        {
            var usersList = await _userRepository.GetAllAsync();

            if (usersList is null)
                return NotFound();

            return Ok(usersList);
        }
    }
}