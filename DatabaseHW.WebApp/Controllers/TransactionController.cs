﻿using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DatabaseHW.WebApp.Controllers
{
    [ApiController]
    [Route("sberbank/transactions")]
    public class TransactionController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly ITransactionRepository _transactionRepository;

        public TransactionController(ILogger<UserController> logger,
            ITransactionRepository transactionRepository)
            => (_logger, _transactionRepository) =
                (logger, transactionRepository);

        [HttpPost("add")]
        public async Task<ActionResult> AddTransaction(Transaction transaction)
        {
            await _transactionRepository.AddAsync(transaction);
            await _transactionRepository.SaveAsync();

            return Ok();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetTransaction(int id)
        {
            var transaction = await _transactionRepository.GetAsync(id);

            if (transaction is null)
                return NotFound();

            return Ok(transaction);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTransaction(int id)
        {
            var transaction = await _transactionRepository.GetAsync(id);
            
            if (transaction is null)
                return NotFound();

            await _transactionRepository.DeleteAsync(transaction);
            await _transactionRepository.SaveAsync();

            return Ok();
        }
    }
}