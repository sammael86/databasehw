﻿using System.Linq;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using DatabaseHW.DataGenerator;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace DatabaseHW.WebApp.Controllers
{
    [ApiController]
    [Route("sberbank")]
    public class SberbankController : ControllerBase
    {
        private readonly ILogger<SberbankController> _logger;
        private readonly EFContext _efContext;
        private readonly IRepository<Tariff> _tariffRepository;
        private readonly IRepository<Currency> _currencyRepository;
        private readonly IRepository<CardType> _cardTypeRepository;
        private readonly IRepository<TransactionType> _transactionTypeRepository;

        public SberbankController(ILogger<SberbankController> logger, EFContext efContext,
            IRepository<Tariff> tariffRepository, IRepository<Currency> currencyRepository,
            IRepository<CardType> cardTypeRepository, IRepository<TransactionType> transactionTypeRepository)
            => (_logger, _efContext, _tariffRepository, _currencyRepository, _cardTypeRepository,
                    _transactionTypeRepository) =
                (logger, efContext, tariffRepository, currencyRepository, cardTypeRepository,
                    transactionTypeRepository);

        [HttpPost("initialize")]
        public async Task<IActionResult> Initialize(int amount = 50)
        {
            _logger.LogInformation("Запущена инициализация БД");

            var users = RandomUsersGenerator.Generate(amount);
            var accounts = RandomAccountsGenerator.Generate(amount);
            var cards = RandomCardsGenerator.Generate(accounts.Count);

            await _efContext.Database.EnsureDeletedAsync();
            await _efContext.Database.EnsureCreatedAsync();

            await _efContext.AddRangeAsync(users);
            await _efContext.AddRangeAsync(accounts);
            await _efContext.AddRangeAsync(cards);

            await _efContext.SaveChangesAsync();

            accounts = await _efContext.Accounts.ToListAsync();
            var transactions = RandomTransactionsGenerator.Generate(accounts);
            await _efContext.AddRangeAsync(transactions);

            await _efContext.SaveChangesAsync();

            await _efContext.Accounts.ForEachAsync(a =>
                a.Balance = transactions.Where(t => t.AccountId == a.Id)
                    .Sum(t => t.Sum * (t.TransactionTypeId == 1 ? 1 : -1) * t.Rate));

            await _efContext.SaveChangesAsync();

            _logger.LogInformation("Инициализация БД успешно завершена");

            return Ok();
        }

        [HttpGet("tariffs")]
        public async Task<IActionResult> GetTariffs()
        {
            var tariffList = await _tariffRepository.GetAllAsync();

            return Ok(tariffList);
        }

        [HttpGet("currencies")]
        public async Task<IActionResult> GetCurrencies()
        {
            var currenciesList = await _currencyRepository.GetAllAsync();

            return Ok(currenciesList);
        }

        [HttpGet("cardTypes")]
        public async Task<IActionResult> GetCardTypes()
        {
            var cardTypesList = await _cardTypeRepository.GetAllAsync();

            return Ok(cardTypesList);
        }

        [HttpGet("transactionTypes")]
        public async Task<IActionResult> GetTransactionTypes()
        {
            var transactionTypesList = await _transactionTypeRepository.GetAllAsync();

            return Ok(transactionTypesList);
        }
    }
}