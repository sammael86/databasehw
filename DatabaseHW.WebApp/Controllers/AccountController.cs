﻿using System;
using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DatabaseHW.WebApp.Controllers
{
    [ApiController]
    [Route("sberbank/accounts")]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IAccountRepository _accountRepository;

        public AccountController(ILogger<UserController> logger,
            IAccountRepository accountRepository)
            => (_logger, _accountRepository) =
                (logger, accountRepository);

        [HttpPost("add")]
        public async Task<ActionResult> AddAccount(Account account)
        {
            await _accountRepository.AddAsync(account);
            await _accountRepository.SaveAsync();

            return Ok();
        }

        [HttpGet("{id}/transactions")]
        public async Task<ActionResult> GetAccountTransactions(int id, DateTime? fromDate, DateTime? dueDate)
        {
            fromDate ??= DateTime.Now.AddDays(-60);
            dueDate ??= DateTime.Now;

            var accountTransactionsList =
                await _accountRepository.GetAccountTransactionsAsync(id, fromDate.Value, dueDate.Value);

            if (accountTransactionsList is null)
                return NotFound();

            return Ok(accountTransactionsList);
        }

        [HttpGet("{id}/cards")]
        public async Task<ActionResult> GetAccountCards(int id)
        {
            var accountCardsList = await _accountRepository.GetAccountCardsAsync(id);

            if (accountCardsList is null)
                return NotFound();

            return Ok(accountCardsList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAccount(int id)
        {
            var account = await _accountRepository.GetAsync(id);

            if (account is null)
                return NotFound();

            return Ok(account);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAccount(int id)
        {
            var account = await _accountRepository.GetAsync(id);
            
            if (account is null)
                return NotFound();

            await _accountRepository.DeleteAsync(account);
            await _accountRepository.SaveAsync();

            return Ok();
        }
    }
}