﻿using System.Threading.Tasks;
using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace DatabaseHW.WebApp.Controllers
{
    [ApiController]
    [Route("sberbank/cards")]
    public class CardController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly ICardRepository _cardRepository;

        public CardController(ILogger<UserController> logger,
            ICardRepository cardRepository)
            => (_logger, _cardRepository) =
                (logger, cardRepository);

        [HttpPost("add")]
        public async Task<ActionResult> AddCard(Card card)
        {
            await _cardRepository.AddAsync(card);
            await _cardRepository.SaveAsync();

            return Ok();
        }

        [HttpGet("{id}/account")]
        public async Task<ActionResult> GetCardAccount(int id)
        {
            var accountCardsList = await _cardRepository.GetCardAccountAsync(id);

            if (accountCardsList is null)
                return NotFound();

            return Ok(accountCardsList);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetCard(int id)
        {
            var card = await _cardRepository.GetAsync(id);

            if (card is null)
                return NotFound();

            return Ok(card);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCard(int id)
        {
            var card = await _cardRepository.GetAsync(id);
            
            if (card is null)
                return NotFound();

            await _cardRepository.DeleteAsync(card);
            await _cardRepository.SaveAsync();

            return Ok();
        }
    }
}