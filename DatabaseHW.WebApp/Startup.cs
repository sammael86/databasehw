using DatabaseHW.Core.Entities;
using DatabaseHW.Core.Repositories;
using DatabaseHW.DataAccess.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using EF = DatabaseHW.DataAccess.Repositories.EF;
using ADO = DatabaseHW.DataAccess.Repositories.ADO;

namespace DatabaseHW.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<EFContext>(options =>
                options.UseMySql(Configuration.GetConnectionString("MySQLConnection"),
                    ServerVersion.FromString("8.0.22-mysql")));
            services.AddStackExchangeRedisCache(options =>
                options.Configuration = "redis:6379");

            ConfigureRepositories(services);

            services.AddOptions();
            services.Configure<ConnectionStrings>(Configuration.GetSection("ConnectionStrings"));
            services.AddControllers();
            services.AddLogging();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "DatabaseHW.WebApp", Version = "v1"});
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "DatabaseHW.WebApp v1"));
            }

            app.UseRouting();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }

        private void ConfigureRepositories(IServiceCollection services)
        {
            var repositoryType = Configuration.GetValue<string>("REPOSITORY");
            if (repositoryType == "EF")
            {
                services.AddScoped<IUserRepository, EF.UserRepository>();
                services.AddScoped<IAccountRepository, EF.AccountRepository>();
                services.AddScoped<ICardRepository, EF.CardRepository>();
                services.AddScoped<ITransactionRepository, EF.TransactionRepository>();

                services.AddScoped<IRepository<Tariff>, EF.Repository<Tariff>>();
                services.AddScoped<IRepository<Currency>, EF.Repository<Currency>>();
                services.AddScoped<IRepository<CardType>, EF.Repository<CardType>>();
                services.AddScoped<IRepository<TransactionType>, EF.Repository<TransactionType>>();
            }
            else // ADO
            {
                services.AddScoped<IUserRepository, ADO.UserRepository>();
                services.AddScoped<IAccountRepository, ADO.AccountRepository>();
                services.AddScoped<ICardRepository, ADO.CardRepository>();
                services.AddScoped<ITransactionRepository, ADO.TransactionRepository>();

                services.AddScoped<IRepository<Tariff>, ADO.Repository<Tariff>>();
                services.AddScoped<IRepository<Currency>, ADO.CurrencyRepository>();
                services.AddScoped<IRepository<CardType>, ADO.Repository<CardType>>();
                services.AddScoped<IRepository<TransactionType>, ADO.Repository<TransactionType>>();
            }
        }
    }
}